resource "yandex_compute_instance_group" "nginx-instance-group" {
  name               = "nginx-vm-group"
  folder_id          = var.YC_FOLDER_ID
  service_account_id = yandex_iam_service_account.terraform-script-account.id
  instance_template {
    platform_id = "standard-v2"
    resources {
      cores         = 2
      memory        = 0.5
      core_fraction = 5
    }
    scheduling_policy {
      preemptible = true
    }
    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = data.yandex_compute_image.ubuntu.id
      }
    }
    network_interface {
      network_id = yandex_vpc_network.network.id
      subnet_ids = [for subnet in yandex_vpc_subnet.subnet : subnet.id]
      ipv4       = true
      ipv6       = false
      nat        = false
    }
    metadata = {
      ssh-keys = "${var.USER_NAME}:${file("../.ssh/user_ansible.pub")}"
    }
  }
  scale_policy {
    fixed_scale {
      size = length(var.YC_ZONES)
    }
  }
  allocation_policy {
    zones = [for zone in var.YC_ZONES : zone.name]
  }

  deploy_policy {
    max_unavailable = length(var.YC_ZONES)
    max_expansion   = 0
  }

  depends_on = [
    yandex_resourcemanager_folder_iam_binding.terraform-script-folder,
    yandex_vpc_subnet.subnet
  ]
}

resource "yandex_lb_target_group" "nginx-target-group" {
  name      = "nginx-target-group"
  region_id = var.YC_REGION

  dynamic "target" {
    for_each = yandex_compute_instance_group.nginx-instance-group.instances

    content {
      subnet_id = target.value.network_interface.0.subnet_id
      address   = target.value.network_interface.0.ip_address
    }

  }
  depends_on = [
    yandex_resourcemanager_folder_iam_binding.terraform-script-folder,
    yandex_compute_instance_group.nginx-instance-group
  ]
}

output "nginx_servers-internal-ips" {
  value = [for vm in yandex_compute_instance_group.nginx-instance-group.instances : vm.network_interface.0.ip_address]
}
