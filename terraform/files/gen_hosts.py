import json
import sys
from os import path

if __name__ == '__main__':
    output_fn = '../ansible/'
    hosts = {}
    # get command line vars
    data = json.loads(''.join(sys.argv[1:]))
    for key, value in data.items():
        if 'internal' in key:
            group = key.split('-')[0]
            if value['type'] == 'string':
                ips = [value['value']]
            elif 'tuple' in value['type']:
                ips = []
                for ip in value['value']:
                    ips.append(ip)
            else:
                ips = []
            hosts[group] = ips
    
    for group, ips in hosts.items():
        host_data = f"[{group}]\n"
        ips_data = [
            f"{group}_{x + 1} ansible_host={ips[x]}" for x in range(len(ips))]
        host_data += '\n'.join(ips_data)
        print(host_data)
        if path.exists(output_fn):
            with open(path.join(output_fn, f'hosts-{group}'), 'w') as f:
                f.write(host_data)


    
