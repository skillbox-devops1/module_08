# Yandex compute instance for ReactJS
resource "yandex_compute_instance" "yci-react-js" {
  name        = "react-js-vm"
  folder_id   = var.YC_FOLDER_ID
  platform_id = "standard-v2"
  zone        = var.YC_ZONES[0].name
  resources {
    cores         = 2
    memory        = 2
    core_fraction = 5
  }
  scheduling_policy {
    preemptible = true
  }
  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu.id
      size     = 10
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet[var.YC_ZONES[0].name].id
    ipv4      = true
    ipv6      = false
    nat       = false
  }
  metadata = {
    ssh-keys = "${var.USER_NAME}:${file(var.PUBLIC_PATH)}"
  }
  depends_on = [
    yandex_vpc_subnet.subnet
  ]
}

output "react_servers-internal-ip" {
  value = yandex_compute_instance.yci-react-js.network_interface.0.ip_address
}
