# Yandex compute instance for Ansible
resource "yandex_compute_instance" "yci-ansible" {
  name        = "ansible-vm"
  folder_id   = var.YC_FOLDER_ID
  platform_id = "standard-v2"
  zone        = var.YC_ZONES[0].name
  resources {
    cores         = 2
    memory        = 0.5
    core_fraction = 5
  }
  scheduling_policy {
    preemptible = true
  }
  boot_disk {
    mode = "READ_WRITE"
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu.id
    }
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet[var.YC_ZONES[0].name].id
    ipv4      = true
    ipv6      = false
    nat       = true
  }
  metadata = {
    ssh-keys = "${var.USER_NAME}:${file(var.PUBLIC_PATH)}"
  }

  depends_on = [
    yandex_vpc_subnet.subnet,
    yandex_compute_instance_group.nginx-instance-group,
    yandex_compute_instance.yci-react-js
  ]
}

output "ansible-vm-nat-ip" {
  value = yandex_compute_instance.yci-ansible.network_interface.0.nat_ip_address
}

resource "null_resource" "yci-ansible" {
  triggers = {
    cluster_instance_ids = yandex_compute_instance.yci-ansible.id
  }
  connection {
    type        = "ssh"
    host        = yandex_compute_instance.yci-ansible.network_interface.0.nat_ip_address
    user        = var.USER_NAME
    agent       = false
    private_key = file(var.PRIVATE_PATH)
  }

  # copy keys
  provisioner "file" {
    source      = var.PUBLIC_PATH
    destination = "/home/${var.USER_NAME}/.ssh/id_rsa.pub"
  }

  provisioner "file" {
    source      = var.PRIVATE_PATH
    destination = "/home/${var.USER_NAME}/.ssh/id_rsa"
  }

  # copy ansible config
  provisioner "file" {
    source      = "./files/.ansible.cfg"
    destination = "/home/${var.USER_NAME}/.ansible.cfg"
  }
  # local-exec
  provisioner "local-exec" {
    command = "python3 files/gen_hosts.py $(terraform output -json)"
  }

  # copy ansible files
  provisioner "file" {
    source      = "../ansible"
    destination = "/home/${var.USER_NAME}/"
  }

  # install ansible and run
  provisioner "remote-exec" {
    inline = [
      "chmod 0600 /home/${var.USER_NAME}/.ssh/*",
      "sudo apt install ansible -y",
      "echo '\n\n\tInfra created! Ansible begin!\n\n'",
      "ansible-playbook /home/${var.USER_NAME}/ansible/main.yml"
    ]
  }
}
