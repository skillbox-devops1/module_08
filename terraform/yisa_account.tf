# Service account to manage resources

# Create service account
resource "yandex_iam_service_account" "terraform-script-account" {
  name        = "terraform-script-account"
  description = "Terraform service account for compute instance group."
}

# Set service group
resource "yandex_resourcemanager_folder_iam_binding" "terraform-script-folder" {
  folder_id = var.YC_FOLDER_ID
  role      = "editor"
  members = [
    "serviceAccount:${yandex_iam_service_account.terraform-script-account.id}",
    "serviceAccount:${data.yandex_iam_service_account.terraform.id}"
  ]
  depends_on = [
    yandex_iam_service_account.terraform-script-account,
    data.yandex_iam_service_account.terraform
  ]
}
