# File with variables

# cloud id
variable "YC_CLOUD_ID" {
  type        = string
  nullable    = false
  description = ""
}

# folder id
variable "YC_FOLDER_ID" {
  type        = string
  nullable    = false
  description = ""
}

# token
variable "YC_TOKEN" {
  type        = string
  nullable    = false
  description = ""
}

# region
variable "YC_REGION" {
  type    = string
  default = "ru-central1"
}

# zones
variable "YC_ZONES" {
  type = list(object({
    name : string
    subnet : string
    cidr : string
  }))
  default = [{
    name   = "ru-central1-a"
    subnet = "subnet1-a"
    cidr   = "192.168.100.0/24"
  }]
  description = "List of objects (name - zone name, subnet - subnet name, cidr - subnet cird)."
  validation {
    condition     = length(var.YC_ZONES[0].name) > 0 && length(trimprefix(var.YC_ZONES[0].name, "ru-central1-")) < length(var.YC_ZONES[0].name)
    error_message = "Zone names must starts with \"ru-central1-\"."
  }
}

# application port
variable "APP_PORT" {
  type    = number
  default = 80
}

# login
variable "YC_LOGIN" {
  type        = string
  nullable    = false
  description = "Login for adding Terraform user in iam group."
  validation {
    condition     = length(var.YC_LOGIN) > 0
    error_message = "Enter valid Yandex Cloud login."
  }
}

# keys
variable "PRIVATE_PATH" {
  type        = string
  nullable    = false
  description = "Relative path to private key file path."
}
variable "PUBLIC_PATH" {
  type        = string
  nullable    = false
  description = "Relative path to private key file path."
}

variable "USER_NAME" {
  type    = string
  default = "ubuntu"
}

variable "IMAGE_FAMILY" {
  type    = string
  default = "ubuntu-1604-lts"
}
