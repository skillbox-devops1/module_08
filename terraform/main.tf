# Set YC terraform provider
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

# set provider
provider "yandex" {
  cloud_id  = var.YC_CLOUD_ID
  folder_id = var.YC_FOLDER_ID
  token     = var.YC_TOKEN
  zone      = var.YC_ZONES[0].name
}

# get last Ubuntu 20.04 image
data "yandex_compute_image" "ubuntu" {
  family = "ubuntu-2204-lts"
}

data "yandex_iam_user" "terraform" {
  login = var.YC_LOGIN
}
