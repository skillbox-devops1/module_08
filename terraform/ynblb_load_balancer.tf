# 
resource "yandex_lb_network_load_balancer" "nginx-load-balancer" {
  name      = "nginx-load-balancer"
  region_id = var.YC_REGION

  listener {
    name        = "my-listener"
    protocol    = "tcp"
    port        = var.APP_PORT
    target_port = var.APP_PORT
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.nginx-target-group.id
    healthcheck {
      name                = "http"
      healthy_threshold   = 2
      interval            = 70
      timeout             = 60
      unhealthy_threshold = 10
      http_options {
        port = var.APP_PORT
        path = "/"
      }
    }
  }

  depends_on = [
    yandex_resourcemanager_folder_iam_binding.terraform-script-folder,
    yandex_lb_target_group.nginx-target-group
  ]
}

output "lb-external-ip" {
  value = [for l in yandex_lb_network_load_balancer.nginx-load-balancer.listener : l.external_address_spec.*.address].0[0]
}
