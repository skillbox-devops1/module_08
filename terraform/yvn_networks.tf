# Network
resource "yandex_vpc_network" "network" {
  name      = "9-ansible-test-network"
  folder_id = var.YC_FOLDER_ID
  depends_on = [
    yandex_resourcemanager_folder_iam_binding.terraform-script-folder
  ]
}

# Subnets
resource "yandex_vpc_subnet" "subnet" {
  for_each = { for zone in var.YC_ZONES : zone.name => zone }

  zone           = each.value.name
  network_id     = yandex_vpc_network.network.id
  name           = "9-ansible-test-${each.value.subnet}"
  v4_cidr_blocks = ["${each.value["cidr"]}"]
  folder_id      = var.YC_FOLDER_ID
  route_table_id = yandex_vpc_route_table.rt.id
  depends_on = [
    yandex_resourcemanager_folder_iam_binding.terraform-script-folder,
    yandex_vpc_network.network,
    yandex_vpc_route_table.rt
  ]
}

# Gateway
resource "yandex_vpc_gateway" "nat_gateway" {
  name = "subnets-gateway"
  shared_egress_gateway {}
  depends_on = [
    yandex_resourcemanager_folder_iam_binding.terraform-script-folder,
    yandex_vpc_network.network
  ]
}

resource "yandex_vpc_route_table" "rt" {
  name       = "subnets-gateway-route-table"
  network_id = yandex_vpc_network.network.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = yandex_vpc_gateway.nat_gateway.id
  }
  depends_on = [
    yandex_resourcemanager_folder_iam_binding.terraform-script-folder,
    yandex_vpc_network.network,
    yandex_vpc_gateway.nat_gateway
  ]
}
