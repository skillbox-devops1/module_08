#!/bin/bash

cd terraform/
terraform plan -destroy -out=.9-ansible.tfplan
terraform apply ".9-ansible.tfplan"
rm .9-ansible.tfplan
