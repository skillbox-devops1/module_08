#!/bin/bash

cd terraform/

if [[ -z .terraform ]]; then
    echo "Initialization Terraform..."
    terraform init
fi

terraform validate
terraform fmt
terraform plan -out=.9-ansible.tfplan
terraform apply ".9-ansible.tfplan"

# destroy ansible vm
terraform plan -destroy -target yandex_compute_instance.yci-ansible -out=.9-ansible.tfplan
terraform apply ".9-ansible.tfplan"

echo "\n\n\tApp address http://$(terraform output -raw lb-external-ip)\n\n"
